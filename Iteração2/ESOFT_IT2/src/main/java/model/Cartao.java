package model;

public interface Cartao {

    boolean valida();

    /**
     *
     * @param data
     * @return
     */
    public abstract Colaborador getColaboradorAtribuido(String data);
}
