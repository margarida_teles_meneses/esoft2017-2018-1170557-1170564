/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Empresa {

    private final List<Equipamento> m_lstEquipamentos;

    public Empresa() {
        this.m_lstEquipamentos = new ArrayList<Equipamento>();

        fillInData();
    }

    private void fillInData() {
        // Dados de Teste
        //Preenche alguns Terminais
        for (Integer i = 1; i <= 4; i++) {
            addEquipamento(new Equipamento("Equipamento " + i.toString(), "0" + i.toString(), "", ""));
        }

        //Preencher outros dados aqui
    }

    public Equipamento novoEquipamento() {
        return new Equipamento();
    }

    public boolean validaEquipamento(Equipamento e) {
        boolean bRet = false;
        if (e.valida()) {
           // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaEquipamento(Equipamento e) {
        if (this.validaEquipamento(e)) {
            return addEquipamento(e);
        }
        return false;
    }

    private boolean addEquipamento(Equipamento e) {
        return m_lstEquipamentos.add(e);
    }

    public List<Equipamento> getListaEquipamentos() {
        return this.m_lstEquipamentos;
    }

    public Equipamento getEquipamento(String sEquipamento) {
        for (Equipamento term : this.m_lstEquipamentos) {
            if (term.isIdentifiableAs(sEquipamento)) {
                return term;
            }
        }

        return null;
    }

    public Cartao novoCartao() {
        // TODO - implement Empresa.novoCartao
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param c
     */
    public boolean validaCartao(Cartao c) {
        // TODO - implement Empresa.validaCartao
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param c
     */
    public void registaCartao(Cartao c) {
        // TODO - implement Empresa.registaCartao
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param c
     */
    public void addCartao(Cartao c) {
        // TODO - implement Empresa.addCartao
        throw new UnsupportedOperationException();
    }

    public Perfil novoPerfil() {
        // TODO - implement Empresa.novoPerfil
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param p
     */
    public boolean validaPerfil(Perfil p) {
        // TODO - implement Empresa.validaPerfil
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param p
     */
    public void registaPerfil(Perfil p) {
        // TODO - implement Empresa.registaPerfil
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param p
     */
    public void addPerfil(Perfil p) {
        // TODO - implement Empresa.addPerfil
        throw new UnsupportedOperationException();
    }

    public Colaborador novoColaborador() {
        // TODO - implement Empresa.novoColaborador
        throw new UnsupportedOperationException();
    }

    public String getPerfis() {
        // TODO - implement Empresa.getPerfis
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param co
     */
    public boolean validaColaborador(Colaborador co) {
        // TODO - implement Empresa.validaColaborador
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param co
     */
    public void registaColaborador(Colaborador co) {
        // TODO - implement Empresa.registaColaborador
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param co
     */
    public void addColaborador(Colaborador co) {
        // TODO - implement Empresa.addColaborador
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param numMeca
     */
    public Colaborador procurarColaborador(String numMeca) {
        // TODO - implement Empresa.procurarColaborador
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param dataInicio
     * @param dataFim
     */
    public Cartao[] obterListaCartoes(String dataInicio, String dataFim) {
        // TODO - implement Empresa.obterListaCartoes
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param numIdent
     */
    public Cartao getCartao(String numIdent) {
        // TODO - implement Empresa.getCartao
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param dataInicio
     * @param dataFim
     * @param dataHoraAtribuicao
     */
    public void setDados(String dataInicio, String dataFim, String dataHoraAtribuicao) {
        // TODO - implement Empresa.setDados
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param a
     */
    public boolean validaAtribuicao(AtribuicaoCartao a) {
        // TODO - implement Empresa.validaAtribuicao
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param a
     */
    public boolean registaAtribuicao(AtribuicaoCartao a) {
        // TODO - implement Empresa.registaAtribuicao
        throw new UnsupportedOperationException();
    }

    public void create() {
        // TODO - implement Empresa.create
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param numIdent
     * @param dataEm
     * @param versao
     */
    public Cartao getCartao(String numIdent, String dataEm, String versao) {
        // TODO - implement Empresa.getCartao
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param endLogico
     * @param endFisico
     * @param descricao
     * @param fxConf
     */
    public void getEquipamento(String endLogico, String endFisico, String descricao, String fxConf) {
        // TODO - implement Empresa.getEquipamento
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param eq
     * @param e
     */
    public boolean validaEquip(Equipamento[] eq, Equipamento e) {
        // TODO - implement Empresa.validaEquip
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param ac
     */
    public boolean validaAcesso(AcessoAreaRestrita ac) {
        // TODO - implement Empresa.validaAcesso
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param ac
     */
    public void registaAcesso(AcessoAreaRestrita ac) {
        // TODO - implement Empresa.registaAcesso
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param ac
     */
    public void addAcesso(AcessoAreaRestrita ac) {
        // TODO - implement Empresa.addAcesso
        throw new UnsupportedOperationException();
    }

}
