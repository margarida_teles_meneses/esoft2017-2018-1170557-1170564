/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.model;

/**
 *
 * @author marga
 */
public class Colaborador extends Empresa{

    private String numMec;
    private Perfil perfil;
    private String nomeComp;
    private String nomeAbr;

    /**
     *
     * @param numMec
     */
    public void setNumeroMecanografico(String numMec) {
        this.numMec = numMec;
    }

    /**
     *
     * @param nomeComp
     */
    public void setNomeCompleto(String nomeComp) {
        this.nomeComp = nomeComp;
    }
    
    public void setNomeAbreviado(String nomeAbr){
        this.nomeAbr = nomeAbr;
    }

    /**
     *
     * @param perfil
     */
    public void setPerfilAutorizacao(Perfil perfil) {
       this.perfil = perfil;
    }

    public boolean valida() {
        // TODO - implement Colaborador.valida
        throw new UnsupportedOperationException();
    }

    public Perfil getPerfil() {
        return this.perfil;
    }

}
