package Model;

public interface AtribuicaoCartao {

    /**
     *
     * @param data
     */
    boolean pertencePeriodo(String data);

    Colaborador getColaborador();

    boolean valida();

}
