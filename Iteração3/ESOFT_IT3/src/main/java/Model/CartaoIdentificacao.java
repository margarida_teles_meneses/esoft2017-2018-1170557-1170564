package Model;

public interface CartaoIdentificacao {

    /**
     *
     * @param data
     * @return 
     */
    Colaborador getColaboradorAtribuido(String data);

    /**
     *
     * @param cartao_id
     * @return 
     */
    boolean isCartao(int cartao_id);

    boolean eDefinitivo();

    boolean eProvisorio();

    /**
     *
     * @param colab
     * @param dataInicio
     * @return 
     */
    AtribuicaoCartaoDefinitivo novaAtribuicao(int colab, int dataInicio);

    /**
     *
     * @param novaAC
     */
    void registaAtribuicao(int novaAC);

    /**
     *
     * @param novaAC
     */
    boolean validaAtribuicao(int novaAC);

    /**
     *
     * @param c
     * @param dataFim
     */
    AtribuicaoCartao novaAtribuicao(Colaborador c, String dataFim);

    /**
     *
     * @param atr
     */
    boolean validaAtribuicao(AtribuicaoCartao atr);

    /**
     *
     * @param atr
     */
    boolean registaAtribuicao(AtribuicaoCartao atr);

    AtribuicaoCartao getAtribuicao();
}
