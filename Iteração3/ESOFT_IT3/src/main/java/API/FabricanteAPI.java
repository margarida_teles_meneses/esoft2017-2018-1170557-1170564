package API;

import java.util.List;

public interface FabricanteAPI {

    List getModelos();

    /**
     *
     * @param endLogico
     */
    boolean validaEnderecoLogico(String endLogico);

    /**
     *
     * @param modelo
     */
    boolean validaModelo(String modelo);

    /**
     *
     * @param c
     * @param listpa
     */
    void set(int c, int listpa);

}
